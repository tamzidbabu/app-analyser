package xyz.socian.app_analyzer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.Utils.ApplicationSingleton;
import xyz.socian.app_analyzer.Utils.Constant;
import xyz.socian.app_analyzer.Utils.RetrofitApiClient;
import xyz.socian.app_analyzer.Utils.RetrofitApiInterface;
import xyz.socian.app_analyzer.Utils.SharedPreferencesData;
import xyz.socian.app_analyzer.adapter.ChatRecyclerAdapter;
import xyz.socian.app_analyzer.model.ChatActivityUsers;
import xyz.socian.app_analyzer.model.CommentUserModel;
import xyz.socian.app_analyzer.model.User;


/**
 * Created by ahmed on 12-02-18.
 */

public class ChatActivity extends AppCompatActivity {
    ChatRecyclerAdapter adapter;
    @BindView(R.id.chat_layout_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.sender_edit_text)
    EditText chatText;
    public static String user = "me";//subject to change
    ArrayList<String> sentChatText, receivedChatText;
    @BindView(R.id.chat_text_send_button)
    ImageView sendButton;
    String incomingUserId, incomingCommentId,text;
    ChatActivityUsers sender;
    CommentUserModel otherUser;
    RetrofitApiInterface apiService;
    String token;
    public SharedPreferencesData sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);
        sender = new ChatActivityUsers();
        otherUser=new CommentUserModel();
        sentChatText = new ArrayList<>();
        receivedChatText = new ArrayList<>();
        sharedpreferences=new SharedPreferencesData();

        DB snappydb;
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 5; i++) {
            receivedChatText.add("test");
        }
        incomingUserId = getIntent().getStringExtra("userId");
        int CommentId = getIntent().getIntExtra("commentId", 0);
        incomingCommentId = String.valueOf(CommentId);

        otherUser= (CommentUserModel) getIntent().getSerializableExtra("MyClass");

        sendButton = (ImageView) findViewById(R.id.chat_text_send_button);
        chatText = (EditText) findViewById(R.id.sender_edit_text);
        apiService = RetrofitApiClient.getClient().create(RetrofitApiInterface.class);

        Call<ChatActivityUsers> call = apiService.getChatMessage(incomingUserId);
        call.enqueue(new Callback<ChatActivityUsers>() {

            @Override
            public void onResponse(Call<ChatActivityUsers> call, Response<ChatActivityUsers> response) {
                //INSERT MESSAGE RECEIVING CODE HERE USING response.body()
                if (response.body() != null) {
                    sharedpreferences.addSharedPrefMessage(null, otherUser, "myPref");
                }
            }

            @Override
            public void onFailure(Call<ChatActivityUsers> call, Throwable t) {

            }
        });
//        @Override
//        public void onResponse(Call<ArrayList<ChatActivityUsers>> call, Response<ArrayList<ChatActivityUsers>> response) {
//            if (response.isSuccessful()) {
//                news =  response.body();
//                obj=StorageSingleton.getInstance();
//                obj.setLatestNewsData(news);
//                Log.i("Response","Response :" +response.body().toString());
//                adapter.notifyDataSetChanged();
//                adapter = new NewsAdapter(news, R.layout.list_item_news, con, home);
//                recyclerView.setAdapter(adapter);
//                //adapter.setData(news);
//                Log.d(TAG, "Number of news received: " + news.size());
//            }
//            else{
//                System.out.println("response failed!!");
//            }
//        }
//
//        @Override
//        public void onFailure(Call<ArrayList<ChatActivityUsers>> call, Throwable t) {
//
//        }


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chatText.getText().toString().isEmpty()) {


                    sharedpreferences.addSharedPrefMessage(sender,null,"myPref");

                    sentChatText.add(chatText.getText().toString());

                    initRecyclerView();

                    adapter = new ChatRecyclerAdapter(sentChatText, getApplicationContext());
                    recyclerView.setAdapter(adapter);

                    apiService.sendChatMessage("Token " + token, sender.getSenderUserId(), sender.getChatText(), sender.isAdmin(), sender.getQuestionId()).enqueue(new Callback<ChatActivityUsers>() {
                        @Override
                        public void onResponse(Call<ChatActivityUsers> call, Response<ChatActivityUsers> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "post submitted to API.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ChatActivityUsers> call, Throwable t) {
                            // Log error here since request failed
                            // Log.e(TAG, t.toString());
                        }
                    });


                }
            }
        });
        chatText.setText("");
    }

    void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.chat_layout_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
//        adapter = new ChatRecyclerAdapter(sentChatText, getApplicationContext());
//        recyclerView.setAdapter(adapter);
    }
}
