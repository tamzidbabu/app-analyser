package xyz.socian.app_analyzer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.api.APICONSTATN;
import xyz.socian.app_analyzer.model.User;
import xyz.socian.app_analyzer.Utils.ApplicationSingleton;
import xyz.socian.app_analyzer.Utils.CommonActivity;
import xyz.socian.app_analyzer.Utils.Constant;

public class LoginActivity extends CommonActivity {
    //
    @BindView(R.id.user_name)
    EditText userName;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login_btn)
    AppCompatButton loginBtn;
    @BindView(R.id.signup_btn)
    AppCompatButton sigupBtn;
    @BindView(R.id.forgot_pass)
    TextView forgotPassView;
    String token;
    public static final String TAG = "LOG_IN";
    SweetAlertDialog pogressDialiog;
    User userModel;
    DB snappydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


//
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            String token = model.getToken();

            if (!token.isEmpty()) {
                startActivity(new Intent(this, MainActivity.class));
                finish();


            }
            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


//        sigupBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
//                startActivity(intent);
//
//            }
//        });


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                pogressDialiog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();


                if (!isOnline()) {
                    pogressDialiog.dismiss();

                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!!!")
                            .setContentText("Please Activate Your Internet Connection")
                            .setConfirmText("Okay!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }


                AsyncHttpClient client = new AsyncHttpClient();

                RequestParams params = new RequestParams();
                params.put("username", userName.getText().toString());
                params.put("password", password.getText().toString());
                client.post(APICONSTATN.LOG_IN, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                        pogressDialiog.dismiss();
                        if (statusCode == 200) {


                            try {
                                String token = response.getString("token");

                                if (!token.isEmpty()) {

                                    userModel = new User();
                                    userModel.setToken(token);

                                    try {

                                        snappydb.put(Constant.USER_MODEL, userModel);


                                        User model = new User();
                                        model = snappydb.get(Constant.USER_MODEL, User.class);

                                        token = model.getToken();


                                        Log.i(TAG, "TOKEN:" + token);

                                        snappydb.close();

                                    } catch (SnappydbException e) {
                                        e.printStackTrace();
                                    }

                                    userApi(token);
                                    posts(token);

                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Success!!!")
                                            .setContentText("You logged in successfully")
                                            .setConfirmText("GO")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                    sweetAlertDialog.dismiss();
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            })
                                            .show();


                                } else {

                                    pogressDialiog.dismiss();
                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Opss!!!")
                                            .setContentText("Sorry, something went wrong.")
                                            .setConfirmText("try again")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.dismiss();
                                                }
                                            })
                                            .show();
                                }



                                //////////////////////////////////////

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {

                            pogressDialiog.dismiss();
                            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Opss!!!")
                                    .setContentText("Sorry, something went wrong.")
                                    .setConfirmText("try again")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }
                });

            }
        });


        forgotPassView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

}
