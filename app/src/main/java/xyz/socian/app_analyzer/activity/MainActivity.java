package xyz.socian.app_analyzer.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.adapter.HomePagerAdapter;
import xyz.socian.app_analyzer.model.ChatActivityUsers;
import xyz.socian.app_analyzer.services.GetRepeatData;
import xyz.socian.app_analyzer.Utils.CommonActivity;
import xyz.socian.app_analyzer.Utils.permissions.Utility;

public class MainActivity extends CommonActivity {


    HomePagerAdapter pagerAdapter;
    GetRepeatData alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarText = (TextView) findViewById(R.id.toolbar_text);
        if (toolbarText != null && toolbar != null) {
            toolbarText.setText("App Analyzer");
            setSupportActionBar(toolbar);
        }

        Utility.checkAndRequestPermissions(this);

        alarm = new GetRepeatData();
        startRepeatingTimer();

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        pagerAdapter = new HomePagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(pagerAdapter);
        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(vpPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.rss_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.message_icon);

        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
//                Toast.makeText(MainActivity.this,
//                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
        LocalBroadcastManager.getInstance(this).registerReceiver(toChatActivity,
                new IntentFilter("chosen chat"));
    }
    public BroadcastReceiver toChatActivity=new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
    //in completion this part will send user id to chat activity.
        Intent i=new Intent(MainActivity.this, ChatActivity.class);
        startActivity(i);
    }
    };





    public void startRepeatingTimer() {

        Context context = this.getApplicationContext();

        if (alarm != null) {

            alarm.SetAlarm(context);

        } else {

            Toast.makeText(context, "data is not available for this moment,contact us!", Toast.LENGTH_SHORT).show();

        }
    }


    public void cancelRepeatingTimer(View view) {
        Context context = this.getApplicationContext();

        if (alarm != null) {

            alarm.CancelAlarm(context);

        } else {

            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();

        }

    }

    public void onetimeTimer(View view) {

        Context context = this.getApplicationContext();

        if (alarm != null) {

            alarm.setOnetimeTimer(context);

        } else {

            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();

        }

    }

}
