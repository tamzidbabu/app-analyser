package xyz.socian.app_analyzer.activity.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.activity.LoginActivity;
import xyz.socian.app_analyzer.activity.MainActivity;
import xyz.socian.app_analyzer.adapter.PostsRecyclerViewAdapter;
import xyz.socian.app_analyzer.api.APICONSTATN;
import xyz.socian.app_analyzer.model.CommentUserModel;
import xyz.socian.app_analyzer.model.PostsModel;
import xyz.socian.app_analyzer.model.User;
import xyz.socian.app_analyzer.Utils.ApplicationSingleton;
import xyz.socian.app_analyzer.Utils.CommonActivity;
import xyz.socian.app_analyzer.Utils.Constant;
import xyz.socian.app_analyzer.Utils.SharedPreferencesData;

import static android.app.Activity.RESULT_OK;
import static xyz.socian.app_analyzer.activity.LoginActivity.TAG;

/**
 * Created by tamzid on 2/11/18.
 */

public class FeedFragment extends Fragment implements ImageChooserListener {
    private String title;
    private int page;
    int REQUEST_CODE = 0;
    Uri imageUri = null;
    ImageView uploaded_img;
    String token;
    User userModel;
    DB snappydb;
    RecyclerView recyclerView;
    List<PostsModel> postsModelList = new ArrayList<>();
    PostsRecyclerViewAdapter adapter;
    //    ImageChooserManager imageChooserManager;
    String imageFilePath;
    SharedPreferencesData sharedPreferencesData;

    private int chooserType;
    private ImageChooserManager imageChooserManager;
    private String filePath;
    private String originalFilePath = null;

    SweetAlertDialog pogressDialiog;


    void openImageChooser() {

        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
//            pbar.setVisibility(View.VISIBLE);
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        pDialog.show();

    }

    // newInstance constructor for creating fragment with arguments
    public static MessageFragment newInstance(int page, String title) {
        MessageFragment fragmentFirst = new MessageFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        sharedPreferencesData = new SharedPreferencesData();
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View item_view = inflater.inflate(R.layout.feed_fragment, container, false);

        Button image_upload_btn = (Button) item_view.findViewById(R.id.image_upload_btn);
        Button post_new_ques = (Button) item_view.findViewById(R.id.post_new_ques);
        final EditText new_post_edit_text = (EditText) item_view.findViewById(R.id.new_post_edit_text);
        uploaded_img = (ImageView) item_view.findViewById(R.id.uploaded_img);


        image_upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openImageChooser();
            }
        });


        post_new_ques.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View view) {
                pogressDialiog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Uploading,Please wait...");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();


                if (originalFilePath == null) {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("No Image!!!")
                            .setContentText("Please select an image")
                            .setConfirmText("Okay!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }
                if (new_post_edit_text.getText().toString().isEmpty() || new_post_edit_text.getText().toString() == null) {
                    new_post_edit_text.setError("Type a question ");
                    return;
                }

                File file = new File(originalFilePath);

                try {
                    DB snappydb = DBFactory.open(ApplicationSingleton.getContext());

                    User model = new User();
                    model = snappydb.get(Constant.USER_MODEL, User.class);

                    token = model.getToken();
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }


                AsyncHttpClient client = new AsyncHttpClient();
                client.addHeader("Authorization", "Token " + token);
//                client.addHeader("Content-Type", "multipart/form-data");

                RequestParams params = new RequestParams();
                try {
                    params.put("image", file);
                    params.put("text", new_post_edit_text.getText().toString());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }


                client.post(APICONSTATN.POST_NEW_QUESTION, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());
//                        if (statusCode == 200) {

                        uploaded_img.setVisibility(View.GONE);
                        new_post_edit_text.setText("");
                        new_post_edit_text.setHint("Write a question");

                        CommonActivity commonActivity = new CommonActivity();
                        commonActivity.posts(token);

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                postsModelList = sharedPreferencesData.getSharedPrefAllPost(Constant.POST);
                                adapter = new PostsRecyclerViewAdapter(postsModelList, getActivity());
                                recyclerView.setAdapter(adapter);
                                pogressDialiog.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Success!!!")
                                        .setContentText("Question uploaded successfully")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                                sweetAlertDialog.dismiss();
                                                adapter.notifyDataSetChanged();
//                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                startActivity(intent);
//                                                onFinish();
                                            }
                                        })
                                        .show();
                            }
                        }, 2000);


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }
                });

            }
        });


        recyclerView = (RecyclerView) item_view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        sharedPreferencesData = new SharedPreferencesData();
        postsModelList = new ArrayList<>();
        postsModelList = sharedPreferencesData.getSharedPrefAllPost(Constant.POST);
        if (sharedPreferencesData.getSharedPrefAllPost(Constant.POST) != null) {
            adapter = new PostsRecyclerViewAdapter(postsModelList, getActivity());
            recyclerView.setAdapter(adapter);
        } else {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    adapter = new PostsRecyclerViewAdapter(postsModelList, getActivity());
                    recyclerView.setAdapter(adapter);
                }
            }, 5000);
        }

        return item_view;
    }


//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        try {
//            switch (requestCode) {
//
//                case 0:
//                    if (resultCode == Activity.RESULT_OK) {
//                        //data gives you the image uri. Try to convert that to bitmap
//                        imageUri = data.getData();
//                        uploaded_img.setVisibility(View.VISIBLE);
//                        uploaded_img.setImageURI(imageUri);
//
//                        break;
//                    } else if (resultCode == Activity.RESULT_CANCELED) {
//                        Log.e(TAG, "Selecting picture cancelled");
//                    }
//                    break;
//            }
//        } catch (Exception e) {
//            Log.e(TAG, "Exception in onActivityResult : " + e.getMessage());
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i(TAG, "OnActivityResult");
        Log.i(TAG, "File Path : " + filePath);
        Log.i(TAG, "Chooser Type: " + chooserType);
        if (resultCode == RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        } else {
//            pbar.setVisibility(View.GONE);
//            pDialog.dismiss();
        }
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    public String getPath(Uri uri) {
        String path = null;
        String[] projection = {MediaStore.MediaColumns.DATA};

        ContentResolver cr = getContext().getContentResolver();
        Cursor metaCursor = cr.query(uri, projection, null, null, null);
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    path = metaCursor.getString(0);
                }
            } finally {
                metaCursor.close();
            }
        }
        return path;
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.i(TAG, "Chosen Image: O - " + chosenImage.getFilePathOriginal());
                Log.i(TAG, "Chosen Image: T - " + chosenImage.getFileThumbnail());
                Log.i(TAG, "Chosen Image: Ts - " + chosenImage.getFileThumbnailSmall());

                originalFilePath = chosenImage.getFilePathOriginal();
//                thumbnailFilePath = chosenImage.getFileThumbnail();
//                thumbnailSmallFilePath = chosenImage.getFileThumbnailSmall();
//                pbar.setVisibility(View.GONE);
                if (chosenImage != null) {
                    Log.i(TAG, "Chosen Image: Is not null");
//                    capturedPhotoImageView.setVisibility(View.VISIBLE);
//                    loadImage(imgview, chosenImage.getFilePathOriginal());
//                    loadImage(imageViewThumbSmall, chosenImage.getFileThumbnailSmall());
                    uploaded_img.setVisibility(View.VISIBLE);
                    File f = new File(originalFilePath);
                    Picasso.with(getActivity())
                            .load(f)
                            .fit()
                            .centerInside()
                            .into(uploaded_img);

                } else {
                    Log.i(TAG, "Chosen Image: Is null");
                }
//                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }
}
