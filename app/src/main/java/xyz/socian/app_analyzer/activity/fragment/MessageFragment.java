package xyz.socian.app_analyzer.activity.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.adapter.MessageFragmentAdapter;

/**
 * Created by tamzid on 2/11/18.
 */

public class MessageFragment extends Fragment {
    MessageFragmentAdapter adapter;
    RecyclerView recyclerView;
    ArrayList<String> dummyNames;
    View view;
    private String title;
    private int page;


    // newInstance constructor for creating fragment with arguments
    public static MessageFragment newInstance(int page, String title) {
        MessageFragment fragmentFirst = new MessageFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.message_fragment, container, false);
        dummyNames = new ArrayList<>();
        for (int i = 0; i <= 4; i++) {
            dummyNames.add("User");
        }
        recyclerView = (RecyclerView) view.findViewById(R.id.message_fragment_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new MessageFragmentAdapter(dummyNames, getContext());
        recyclerView.setAdapter(adapter);
//        TextView tvLabel = (TextView) view.findViewById(R.id.tvLabel);
//        tvLabel.setChatText(page + " -- " + title);
        return view;
    }
}
