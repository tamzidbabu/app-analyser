package xyz.socian.app_analyzer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.activity.ChatActivity;
import xyz.socian.app_analyzer.model.CommentUserModel;

/**
 * Created by tamzid on 2/11/18.
 */

public class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.ViewHolder> {

    private List<CommentUserModel> items;
    private Context context;


    public CommentsRecyclerAdapter(List<CommentUserModel> items,Context context) {
        this.items = items;
        this.context = context;
    }


    @Override public CommentsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate( R.layout.comments_layout, parent, false);

        return new CommentsRecyclerAdapter.ViewHolder(v);

    }



    @Override public void onBindViewHolder(CommentsRecyclerAdapter.ViewHolder holder, final int position) {

        CommentUserModel item = items.get(position);
//        holder.recyclerView.setHasFixedSize(true);
//        holder.recyclerView.setAdapter(new PostsRecyclerViewAdapter());
//        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
//        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.user_name.setText(item.getUserName());
        holder.comments_tv.setText(item.getComment());
        holder.itemView.setTag(item);
        holder.comments_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ChatActivity.class);
                i.putExtra("MyClass", (Serializable) items.get(position));
                i.putExtra("userId",items.get(position).getUserId());
                i.putExtra("commentId",items.get(position).getCommentID());
                context.startActivity(i);
            }
        });
    }



    @Override public int getItemCount() {
        return items.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

//        public ImageView image;


















        public TextView comments_tv;
        public TextView user_name;
        public CardView comments_layout;
        RecyclerView recyclerView ;


        public ViewHolder(View itemView) {
            super(itemView);


//            recyclerView = (RecyclerView) itemView.findViewById(R.id.comment_recycler_view);

//            image = (ImageView) itemView.findViewById(R.id.image);
            comments_layout=(CardView) itemView.findViewById(R.id.comments_layout_cardview);
            comments_tv = (TextView) itemView.findViewById(R.id.comments_tv);
            user_name = (TextView) itemView.findViewById(R.id.user_name);
        }

    }


}
