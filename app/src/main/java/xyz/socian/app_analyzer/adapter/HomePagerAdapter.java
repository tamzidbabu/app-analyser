package xyz.socian.app_analyzer.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import xyz.socian.app_analyzer.activity.fragment.FeedFragment;
import xyz.socian.app_analyzer.activity.fragment.MessageFragment;

/**
 * Created by tamzid on 2/11/18.
 */

public class HomePagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;


    public HomePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                FeedFragment feedFragment = new FeedFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                feedFragment.setArguments(bundle);
//                FeedFragment.newInstance(0, "Page # 1");
                return feedFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title

                MessageFragment messageFragment = new MessageFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putInt("position", position);
                messageFragment.setArguments(bundle1);
//                MessageFragment.newInstance(1, "Page # 2");
                return messageFragment;

            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
//        return "Page " + position;
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return "Feeds";
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return "Message";
            default:
                return null;
        }
    }



}
