package xyz.socian.app_analyzer.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.Utils.SharedPreferencesData;
import xyz.socian.app_analyzer.model.ChatActivityUsers;
import xyz.socian.app_analyzer.model.CommentUserModel;

/**
 * Created by ahmed on 11-02-18.
 */

public class MessageFragmentAdapter extends RecyclerView.Adapter<MessageFragmentAdapter.ViewHolder> {
    private ArrayList<String> listItems;
    SharedPreferencesData sharedPreferencesData=new SharedPreferencesData();
    private Context context;
    String json;


    public MessageFragmentAdapter(ArrayList<String> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_fragment_items, parent, false);



        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MessageFragmentAdapter.ViewHolder holder, final int position) {
        holder.profileName.setText(listItems.get(position));
        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent("chosen chat");
                i.putExtra("position",position);
                LocalBroadcastManager.getInstance(context).sendBroadcast(i);
            }
        });
        json=sharedPreferencesData.getSharedPrefMessage();
        Gson gson = new Gson();
        if (json.contains("senderUserId")) {
            ChatActivityUsers obj = gson.fromJson(json, ChatActivityUsers.class);
            holder.chatTextHead.setText(obj.getChatText());
        }
        else if(json.contains("commentID")){
            CommentUserModel obj = gson.fromJson(json, CommentUserModel.class);
            holder.chatTextHead.setText(obj.getComment());
        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView profileName;
        public TextView chatTextHead;
        RelativeLayout rl;



        public ViewHolder(View itemView) {
            super(itemView);
            rl=(RelativeLayout) itemView.findViewById(R.id.message_fragment_relative_layout);
            profileName = (TextView) itemView.findViewById(R.id.profile_name);
            chatTextHead = (TextView) itemView.findViewById(R.id.chat_text_head);

        }
    }
}
