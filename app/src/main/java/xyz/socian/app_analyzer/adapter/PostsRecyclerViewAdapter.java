package xyz.socian.app_analyzer.adapter;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import xyz.socian.app_analyzer.R;
import xyz.socian.app_analyzer.api.APICONSTATN;
import xyz.socian.app_analyzer.model.CommentUserModel;
import xyz.socian.app_analyzer.model.PostsModel;
import xyz.socian.app_analyzer.model.User;
import xyz.socian.app_analyzer.Utils.ApplicationSingleton;
import xyz.socian.app_analyzer.Utils.Constant;


/**
 * Created by tamzid on 2/11/18.
 */

public class PostsRecyclerViewAdapter extends RecyclerView.Adapter<PostsRecyclerViewAdapter.ViewHolder> {

    private List<PostsModel> items = new ArrayList<>();
    public Context context;
    String token = null;
    int userId;
    SweetAlertDialog pogressDialiog;
    CommentsRecyclerAdapter adapter;

    public PostsRecyclerViewAdapter(List<PostsModel> items, Context context) {
        this.items = items;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.posts_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final PostsModel item = items.get(position);

        User userModel;
        DB snappydb;

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            userId = model.getUserID();
            token = model.getToken();
            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        Picasso.with(context).load(item.getImageLink()).into(holder.image);

        holder.questionText.setText(item.getQuestions());
        List<CommentUserModel> commentUserModelList = new ArrayList<>();

        commentUserModelList = new ArrayList<>();
        commentUserModelList = item.getCommentUserModelList();

        adapter = new CommentsRecyclerAdapter(commentUserModelList, context);
        holder.recyclerView.setAdapter(adapter);

        holder.itemView.setTag(item);

        final List<CommentUserModel> finalCommentUserModelList = commentUserModelList;
        holder.comment_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.comment_input.getText().toString() == null || holder.comment_input.getText().toString().isEmpty()) {
                    holder.comment_input.setError("Write a comment");
                    return;
                }

                pogressDialiog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(context.getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Please wait...");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();

                AsyncHttpClient client = new AsyncHttpClient();
                client.addHeader("Authorization", "Token " + token);
                RequestParams params = new RequestParams();
                params.put("comment_text", holder.comment_input.getText().toString());
                params.put("question", item.getPostId());
                params.put("commenter", userId);

                client.post(APICONSTATN.POST_NEW_COMMENT, params, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        try {
                            int id = response.getInt("id");
                            int question = response.getInt("question");
                            int commenter = response.getInt("commenter");
                            String comment_text = response.getString("comment_text");
                            String commenter_username = response.getString("commenter_username");

                            CommentUserModel commentUserModel = new CommentUserModel();
                            commentUserModel.setCommentID(id);
                            commentUserModel.setPostID(String.valueOf(question));
                            commentUserModel.setUserId(String.valueOf(commenter));
                            commentUserModel.setComment(comment_text);
                            commentUserModel.setUserName(commenter_username);

                            finalCommentUserModelList.add(commentUserModel);
                            items.get(position).setCommentUserModelList(finalCommentUserModelList);
                            pogressDialiog.dismiss();

                            adapter = new CommentsRecyclerAdapter(finalCommentUserModelList, context);
                            holder.recyclerView.setAdapter(adapter);

                            holder.comment_input.setText("");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

//                        CommonActivity commonActivity = new CommonActivity();
//                        commonActivity.posts(token);

//                        final Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//
//
//
////                                commentUserModel.setUserId(item.);
//
//
//
//                            }
//                        }, 1000);
//                        pogressDialiog.dismiss();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        pogressDialiog.dismiss();
                    }
                });

            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView questionText;
        public EditText comment_input;
        RecyclerView recyclerView;
        public Button comment_submit;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.post_image);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.comment_recycler_view);
            questionText = (TextView) itemView.findViewById(R.id.question_text);
            comment_input = (EditText) itemView.findViewById(R.id.comment_input);
            comment_submit = (Button) itemView.findViewById(R.id.comment_submit);


            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(ApplicationSingleton.getContext()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            //            image = (ImageView) itemView.findViewById(R.id.image);
//            text = (TextView) itemView.findViewById(R.id.text);
        }

    }


}
