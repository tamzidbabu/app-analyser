package xyz.socian.app_analyzer.api;

/**
 * Created by tamzid on 2/14/18.
 */

public class APICONSTATN {

    public static String BASEURL = "http://socian-staging.centralindia.cloudapp.azure.com:8000/api/";
    public static String BASEURL_FOR_IMAGE = "http://socian-staging.centralindia.cloudapp.azure.com:8000/";
    public static String LOG_IN = BASEURL+"api-token-auth/";
    public static String USER = BASEURL+"users/";
    public static String GET_ALL_QUESTION = BASEURL+"questions/";
    public static String POST_NEW_QUESTION  = BASEURL+"questions/";
    public static String GET_ALL_COMMENT  = BASEURL+"comments/";
    public static String POST_NEW_COMMENT  = BASEURL+"comments/";
    public static String POST_NEW_MESSAGE  = BASEURL+"messages/";
    public static String GET_ALL_MESSAGE  = BASEURL+"messages/";
    public static String GET_FILTER_MESSAGE  = BASEURL+"messages/?user=1&is_admin_msg=true";
}
