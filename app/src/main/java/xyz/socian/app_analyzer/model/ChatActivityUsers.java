package xyz.socian.app_analyzer.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed on 11-02-18.
 */

public class ChatActivityUsers implements Serializable {
    @SerializedName("user")
    private String senderUserId =null;

    private int receiverUserId=0;
    private String senderUserName=null;
    private String receiverUserName=null;

    @SerializedName("text")
    private String chatText =null;

    @SerializedName("is_admin_msg")
    boolean isAdmin;

    @SerializedName("question")
    int questionId=0;

    public String getSenderUserId() {
        return senderUserId;
    }
    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getChatText() {
        return chatText;
    }

    public void setChatText(String chatText) {
        this.chatText = chatText;
    }
    public int getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(int receiverUserId) {
        this.receiverUserId = receiverUserId;
    }
    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }
    public String getReceiverUserName() {
        return receiverUserName;
    }

    public void setReceiverUserName(String receiverUserName) {
        this.receiverUserName = receiverUserName;
    }
    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
