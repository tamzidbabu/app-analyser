package xyz.socian.app_analyzer.model;

import java.util.List;

/**
 * Created by tamzid on 2/11/18.
 */

public class PostsModel {
    private int postId;
    private String imageLink;

    private String userName;
    private String imageUrl;
    private String imapgeFilePath;
    private String questions;
    private String time;
    private List<CommentUserModel> commentUserModelList;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImapgeFilePath() {
        return imapgeFilePath;
    }

    public void setImapgeFilePath(String imapgeFilePath) {
        this.imapgeFilePath = imapgeFilePath;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<CommentUserModel> getCommentUserModelList() {
        return commentUserModelList;
    }

    public void setCommentUserModelList(List<CommentUserModel> commentUserModelList) {
        this.commentUserModelList = commentUserModelList;
    }
}
