package xyz.socian.app_analyzer.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xyz.socian.app_analyzer.activity.MainActivity;
import xyz.socian.app_analyzer.model.PostsModel;
import xyz.socian.app_analyzer.model.User;
import xyz.socian.app_analyzer.Utils.ApplicationSingleton;
import xyz.socian.app_analyzer.Utils.CommonActivity;
import xyz.socian.app_analyzer.Utils.Constant;
import xyz.socian.app_analyzer.Utils.SharedPreferencesData;

/**
 * Created by tamzid on 2/15/18.
 */

public class GetRepeatData extends BroadcastReceiver {

    final public static String TAG = "GetRepeatData";
    final public static String ONE_TIME = "onetime";
    SharedPreferencesData sharedPreferencesData;
    Context mcontext;
    List<PostsModel> speechContentList = new ArrayList<PostsModel>();
    User userModel;
    DB snappydb;
    String token;

    @Override
    public void onReceive(final Context context, Intent intent) {
//        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");

        sharedPreferencesData = new SharedPreferencesData();
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        //You can do the processing here.

        Bundle extras = intent.getExtras();

        StringBuilder msgStr = new StringBuilder();


        Log.i(TAG, "OnReceive called");

        if (extras != null && extras.getBoolean(ONE_TIME, Boolean.FALSE)) {

            //Make sure this intent has been sent by the one-time timer button.

            msgStr.append("One time Timer : ");

        }

        Format formatter = new SimpleDateFormat("hh:mm:ss a");

        msgStr.append(formatter.format(new Date()));


        System.out.println("Recevied_Time:::" + msgStr);


//        Utility.checkAndRequestPermissions((Activity) context);
// check if GPS enabled
//        gpsTracker = new GPSTracker(context);
////        gpsTracker.getLocation();
//
//        if (gpsTracker.getIsGPSTrackingEnabled())
//        {
//            latitude =gpsTracker.latitude;
//            longitude= gpsTracker.longitude;
//


        CommonActivity  commonActivity = new CommonActivity();
        commonActivity.posts(token);
//        commonActivity.points(Constant.point_speech);
//        commonActivity.points(Constant.point_sentiment);



        //Release the lock
//        wl.release();
    }



    public void SetAlarm(Context context)

    {
        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, GetRepeatData.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),3000, pendingIntent);



    }


    public void CancelAlarm(Context context)

    {

        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, GetRepeatData.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.cancel(pendingIntent);

    }


    public void setOnetimeTimer(Context context) {

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, GetRepeatData.class);

        intent.putExtra(ONE_TIME, Boolean.TRUE);

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);

        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);

    }



}
