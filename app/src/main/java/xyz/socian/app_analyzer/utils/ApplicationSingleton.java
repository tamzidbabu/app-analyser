package xyz.socian.app_analyzer.Utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by tamzid on 1/24/18.
 */

public class ApplicationSingleton extends Application {
    private static ApplicationSingleton instance;

    public static ApplicationSingleton getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
