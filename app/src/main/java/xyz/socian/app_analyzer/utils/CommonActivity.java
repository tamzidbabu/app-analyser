package xyz.socian.app_analyzer.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import xyz.socian.app_analyzer.Utils.SharedPreferencesData;
import xyz.socian.app_analyzer.adapter.PostsRecyclerViewAdapter;
import xyz.socian.app_analyzer.api.APICONSTATN;
import xyz.socian.app_analyzer.model.CommentUserModel;
import xyz.socian.app_analyzer.model.PostsModel;
import xyz.socian.app_analyzer.model.User;

/**
 * Created by tamzid on 2/14/18.
 */

public class CommonActivity extends AppCompatActivity {

    public DB snappydb;
    public SharedPreferencesData sharedPreferencesData = new SharedPreferencesData();

    public PostsModel model;
    public static final String TAG = "CommonActivity";

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isconnected;
        if (netInfo == null || !netInfo.isConnected())
            isconnected = false;
        else
            isconnected = true;
        Log.v("isOnliNe", isconnected + "");
        return isconnected;
    }

    public void postNewQuestionApi() {

    }

    public void userApi(final String mToken) {
        AsyncHttpClient newClient = new AsyncHttpClient();
        newClient.addHeader("Authorization", "Token " + mToken);
        newClient.get(APICONSTATN.USER, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    int userID = response.getInt("id");
                    String userName = response.getString("username");
                    String last_login = response.getString("last_login");
                    boolean is_superuser = response.getBoolean("is_superuser");
                    String first_name = response.getString("first_name");
                    String last_name = response.getString("last_name");
                    String email = response.getString("email");
                    boolean is_staff = response.getBoolean("is_staff");
                    boolean is_active = response.getBoolean("is_active");
                    String date_joined = response.getString("date_joined");
                    String token = mToken;

                    User user = new User();
                    user.setUserID(userID);
                    user.setUserName(userName);
                    user.setLast_login(last_login);
                    user.setIs_superuser(is_superuser);
                    user.setFirst_name(first_name);
                    user.setLast_name(last_name);
                    user.setEmail(email);
                    user.setIs_staff(is_staff);
                    user.setIs_active(is_active);
                    user.setDate_joined(date_joined);
                    user.setToken(mToken);


                    try {

                        snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name


                        snappydb.put(Constant.USER_MODEL, user);


                        User model = new User();
                        model = snappydb.get(Constant.USER_MODEL, User.class);

                        token = model.getToken();


                        Log.i(TAG, "TOKEN:" + token);

                        snappydb.close();

                    } catch (SnappydbException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


            }


        });
    }


    public List<PostsModel> posts(final String mToken) {
        sharedPreferencesData = new SharedPreferencesData();


        final List<PostsModel> postsList = new ArrayList<>();

        AsyncHttpClient newClient = new AsyncHttpClient();
        newClient.addHeader("Authorization", "Token " + mToken);
        newClient.get(APICONSTATN.GET_ALL_QUESTION, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);

                try {

                    sharedPreferencesData.clearDataSharedPref(Constant.POST);
                    response.length();

                    for (int i = 0; i < response.length(); i++) {
                        JSONObject object = response.getJSONObject(i);
                        int id = object.getInt("id");
                        String imageLink = object.getString("image");
                        String text = object.getString("text");

//                        List<CommentUserModel> commentUserModels = new ArrayList<>();
//                        JSONArray array = object.getJSONArray("comments");
//                        int b = array.length();
//                        int c = b;

//                        if (array != null && array.length() >= 0) {
//                            for (int j = 0; j < array.length(); j++) {
//                                JSONObject object1 = array.getJSONObject(i);
//                                int commentId = object1.getInt("id");
//                                String commentText = object1.getString("comment_text");
//                                String question = object1.getString("question");
//                                String commenter = object1.getString("commenter");
//
//                                CommentUserModel commentUserModel = new CommentUserModel();
//                                commentUserModel.setComment(commentText);
//                                commentUserModel.setCommentID(commentId);
//                                commentUserModel.setPostID(question);
//                                commentUserModel.setUserId(commenter);
//                                commentUserModel.setUserName(commenter);
//
//                                commentUserModels.add(commentUserModel);
//
//                            }
//                        }
                        model = new PostsModel();

                        model.setPostId(id);
                        model.setImageLink(APICONSTATN.BASEURL_FOR_IMAGE + imageLink);
                        model.setQuestions(text);
//                        model.setCommentUserModelList(commentUserModels);

//                        sharedPreferencesData.addSharedPrefPost(model, Constant.POST);

                        postsList.add(model);
                        postComment(mToken, postsList.get(i).getPostId(),model);

                    }

//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//
//                        }
//                    }, 5000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });

        return postsList;

    }

    public List<PostsModel> postsModelList = new ArrayList<>();

    List<CommentUserModel> list = new ArrayList<>();


    public PostsModel postsModel = new PostsModel();

    public List<CommentUserModel> postComment(String mToken, int id, final PostsModel model) {


        postsModelList = new ArrayList<>();

        sharedPreferencesData = new SharedPreferencesData();
//        postsModelList = sharedPreferencesData.getSharedPrefAllPost(Constant.POST);
//        for (int i = 0; i < postsModelList.size(); i++) {


        AsyncHttpClient newClient = new AsyncHttpClient();
        newClient.addHeader("Authorization", "Token " + mToken);
//            final int finalI = i;
        newClient.get(APICONSTATN.GET_ALL_COMMENT + "?question=" + id, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
//                        List<PostsModel> postsModelList = new ArrayList<>();
                    list = new ArrayList<>();
                    for (int J = 0; J < response.length(); J++) {
                        JSONObject object = response.getJSONObject(J);
                        int commentid = object.getInt("id");
                        int questionID = object.getInt("question");
                        int commenter = object.getInt("commenter");
                        String comment_text = object.getString("comment_text");
                        String commenter_username = object.getString("commenter_username");

                        CommentUserModel commentUserModel = new CommentUserModel();
                        commentUserModel.setComment(comment_text);
                        commentUserModel.setUserName(commenter_username);


                        list.add(commentUserModel);


                    }
                    model.setCommentUserModelList(list);
                    postsModelList.add(model);
                    sharedPreferencesData.addSharedPrefPost(model, Constant.POST);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


            }


        });
//        }

//        sharedPreferencesData = new SharedPreferencesData();
//        sharedPreferencesData.clearDataSharedPref(Constant.POST);
//        for (int m = 0; m < postsModelList.size(); m++) {
//            PostsModel model1 = postsModelList.get(m);
//            sharedPreferencesData.addSharedPrefPost(model, Constant.POST);
//        }

        return list;

    }
}
