package xyz.socian.app_analyzer.Utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ahmed on 16-02-18.
 */

public class RetrofitApiClient {
    //public static final String BASE_URL = "http://api.themoviedb.org/3/";
    public static final String BASE_URL = "http://socian-staging.centralindia.cloudapp.azure.com:8000/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
