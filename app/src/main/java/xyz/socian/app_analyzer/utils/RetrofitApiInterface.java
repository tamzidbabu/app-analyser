package xyz.socian.app_analyzer.Utils;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import xyz.socian.app_analyzer.model.ChatActivityUsers;

/**
 * Created by ahmed on 16-02-18.
 */

public interface RetrofitApiInterface {
    @GET("api/messages/user/{user}")
    Call<ChatActivityUsers> getChatMessage(@Path("user") String userId);



    @FormUrlEncoded
    @POST("api/messages/")
    Call<ChatActivityUsers> sendChatMessage(@Header("Authorization") String auth, @Field("user") String senderUserId, @Field("text") String chatText,
                                            @Field("is_admin_msg") boolean isAdmin, @Field("question") int questionId);
}
