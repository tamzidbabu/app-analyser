package xyz.socian.app_analyzer.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import xyz.socian.app_analyzer.model.ChatActivityUsers;
import xyz.socian.app_analyzer.model.CommentUserModel;
import xyz.socian.app_analyzer.model.PostsModel;

/**
 * Created by tamzid on 2/15/18.
 */

public class SharedPreferencesData {


    SharedPreferences sharedpreferences;

    public void clearDataSharedPref(String MY_PREFS_NAME) {
        SharedPreferences preferences = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }







    //// to get speech content

    public List<PostsModel> getSharedPrefAllPost(String MY_PREFS_NAME) {
        Gson gson = new Gson();
        List<PostsModel> data = new ArrayList<>();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString("key", "");

        Type type = new TypeToken<List<PostsModel>>() {
        }.getType();
        data = gson.fromJson(jsonPreferences, type);

        return data;
    }

    // to add speech content

    public void addSharedPrefPost(PostsModel addData, String MY_PREFS_NAME) {

        Gson gson = new Gson();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);

        String jsonSaved = sharedPref.getString("key", "");
        String jsonNewproductToAdd = gson.toJson(addData);

        JSONArray jsonArrayProduct = new JSONArray();

        try {
            if (jsonSaved.length() != 0) {
                jsonArrayProduct = new JSONArray(jsonSaved);
            }
            jsonArrayProduct.put(new JSONObject(jsonNewproductToAdd));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //SAVE NEW ARRAY
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("key", String.valueOf(jsonArrayProduct));
        editor.commit();
    }

public void addSharedPrefMessage(ChatActivityUsers sender, CommentUserModel otherUser,String myPref){
    sharedpreferences = ApplicationSingleton.getContext().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    if(sender==null) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(otherUser);
        editor.putString("MyObject", json);
        editor.commit();
    }

    else if(otherUser==null) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(sender);
        editor.putString("MyObject", json);
        editor.commit();
    }

}

public String getSharedPrefMessage() {
    Gson gson = new Gson();
    String json = sharedpreferences.getString("MyObject", "");

    return json;
}

}
